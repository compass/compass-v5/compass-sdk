FROM condaforge/miniforge3:latest

COPY environment.yml /usr/local/conda/

SHELL ["/bin/bash", "-c"]

RUN mamba env create --name compass --file /usr/local/conda/environment.yml \
 && conda init --all \
 && conda clean --all \
 && source ~/.bashrc

# The following is due to an output error with openmpi3
ENV OMPI_MCA_btl_vader_single_copy_mechanism=none

VOLUME /localfs
WORKDIR /localfs

CMD ["/bin/bash"]
